const mysql = require('mysql2/promise');

let connection;
mysql.createConnection({
    host: 'localhost',
    user: 'root',
    database: 'world',
    password: ''
}).then(con => {
    connection = con;
    console.log('Database connected');
}).catch(err => console.error(err));

async function getAllCountries() {
    const [result] = await connection.execute('SELECT * FROM country');
    return result;
}

async function getCountriesByPopulation(min, max) {
    const [result] = await connection.execute('SELECT * FROM country WHERE population > ? AND population < ?', [min, max]);
    return result;
}

module.exports = {
    getAllCountries,
    getCountriesByPopulation
};
