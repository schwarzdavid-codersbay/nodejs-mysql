const express = require('express');
const db = require('./database');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.post('/test', (req, res) => {
	console.log(req.body.firstName);
	res.json({success: true});
});

app.get('/countries', async (req, res) => {
    try {
        const countries = await db.getAllCountries();
        res.json(countries);
    } catch (err) {
        res.status(500).send(err);
    }
});

// http://localhost:8080/countries/200000/9000000
// http://localhost:8080/countries?min=20000&max=90000
app.get('/countries/:min/:max', async (req, res) => {
    try {
        const countries = await db.getCountriesByPopulation(req.params.min, req.params.max);
        res.json(countries);
    } catch (err) {
        res.status(500).send(err);
    }
});

app.listen(8080, err => {
    if(err) {
        console.error('cannot start webserver');
        console.error(err);
        return;
    }
    console.log('Webserver running on port 8080');
});
