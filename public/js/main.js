const countryTable = document.getElementById('countryTableBody');

const refreshBtn = document.getElementById('refresh');
refreshBtn.addEventListener('click', loadCountries);

async function loadCountries() {
    const countryResponse = await fetch('http://localhost:8080/countries');
    const countries = await countryResponse.json();

    countryTable.innerHTML = '';
    for (const country of countries) {
        appendCountryToTable(country);
    }
}

function appendCountryToTable(country) {
    const tr = document.createElement('tr');
    for(let key of ['Code', 'Name', 'Continent', 'Population']) {
        const td = document.createElement('td');
        td.textContent = country[key];
        tr.appendChild(td);
    }
    countryTable.appendChild(tr);
}

async function sendContentToBackend() {
	const firstName = document.getElementById("firstNameInput").value;
	
	const response = await fetch('http://localhost:8080/test', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			firstName: firstName
		});
	});
}